/*
 * This file was generated by qdbusxml2cpp version 0.8
 * Source file was org.bluez.Adapter1.xml
 *
 * qdbusxml2cpp is Copyright (C) The Qt Company Ltd. and other contributors.
 *
 * This is an auto-generated file.
 * This file may have been hand-edited. Look for HAND-EDIT comments
 * before re-generating it.
 */

#include "adapter1_bluez5_p.h"

namespace QtBluetoothPrivate {

/*
 * Implementation of interface class OrgBluezAdapter1Interface
 */

OrgBluezAdapter1Interface::OrgBluezAdapter1Interface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent)
    : QDBusAbstractInterface(service, path, staticInterfaceName(), connection, parent)
{
}

OrgBluezAdapter1Interface::~OrgBluezAdapter1Interface()
{
}

} // end of namespace QtBluetoothPrivate

#include "moc_adapter1_bluez5_p.cpp"
